import 'package:auto_route/auto_route.dart';
import 'package:chat_client_app/core/presentation/app_colors.dart';
import 'package:chat_client_app/core/presentation/app_ui.dart';
import 'package:chat_client_app/core/presentation/widgets/widget_ext.dart';
import 'package:chat_client_app/core/routes/app_router.dart';
import 'package:chat_client_app/core/services/screen_type_detector.dart';
import 'package:chat_client_app/di/injection_container.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';
import 'package:chat_client_app/features/themes_page/data/theme_actioons.dart';
import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';
import 'package:chat_client_app/features/themes_page/presentation/widgets/theme_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// Страница со списком тем
@RoutePage()
class ThemesPage extends StatefulWidget {
  const ThemesPage({Key? key}) : super(key: key);

  @override
  State createState() => _ThemesPageState();
}

class _ThemesPageState extends State<ThemesPage> {
  late ThemeSubscriptionCubit _chatSubscriptionCubit;
  late TextEditingController _titleController;
  final _formKey = GlobalKey<FormState>();
  bool _isRingEnabled = false;

  @override
  void initState() {
    super.initState();
    _titleController = TextEditingController();
    _chatSubscriptionCubit = locator.get<ThemeSubscriptionCubit>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: BlocBuilder<ThemeSubscriptionCubit, ThemeSubscriptionState>(
          bloc: _chatSubscriptionCubit,
          builder: (context, ThemeSubscriptionState state) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Form(
                    key: _formKey,
                    child: ScreenTypeDetector.isHandset(context)
                        ? _buildThemeFormMobile()
                        : _buildThemeFormDesktop()),
                Wrap(
                    children: state.getThemes
                        .map((e) => ThemeWidget(
                              theme: e,
                              onClick: (theme) => context.router.push(ChatRoute(
                                  theme: theme, action: ThemeAction.select)),
                            ))
                        .toList()),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _buildThemeFormDesktop() => Padding(
        padding: const EdgeInsets.only(bottom: 16),
        child: Row(
          children: [
            SizedBox(width: 300, child: _buildTextField()),
            Padding(
                padding: const EdgeInsets.only(top: 20, left: 10),
                child: _buildButton())
          ],
        ),
      );

  Widget _buildThemeFormMobile() => Column(
        children: [
          SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: _buildTextField())
              .paddingOnly(bottom: 16),
          _buildButton()
        ],
      ).paddingOnly(bottom: 16);

  AppBar _buildAppBar() => AppUI.appBar();

  Widget _buildButton() => OutlinedButton(
      style: OutlinedButton.styleFrom(foregroundColor: AppColors.appColor),
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          context.router.push(ChatRoute(
              theme: TalkTheme(isCanRing: _isRingEnabled, title: _titleController.text),
              action: ThemeAction.create));
        }
      },
      child: const Text("Начать диалог"));

  Widget _buildTextField() => Column(
        children: [
          TextFormField(
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Введите название темы';
              }
              return null;
            },
            controller: _titleController,
            decoration: const InputDecoration(hintText: "Введите тему"),
          ),
          CheckboxListTile(title: const Text('Разрешить звонки'), value: _isRingEnabled, onChanged: (val)=>setState(() {
            _isRingEnabled = !_isRingEnabled;
          }))
        ],
      );
}
