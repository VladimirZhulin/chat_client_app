

import 'package:chat_client_app/core/data/websocket/socket_event.dart';
import 'package:chat_client_app/core/socket/base_socket_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';
import 'package:chat_client_app/features/chat_page/domain/cubit/chat_subscription_cubit.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

String userId = '';
String interlocutorId = '';
String creatorId = '';
bool userInChat = false;

/// Кубит для работы со списком тем. Нужен для:
/// 1) Создания тем
/// 2) Выбора темы
/// 3) Удаления темы
class ThemeSubscriptionCubit extends Cubit<ThemeSubscriptionState>{
  ThemeSubscriptionCubit({required this.chatSubscriptionCubit, required this.baseSocketSubscriptionCubit}):super(const ThemeSubscriptionState());
  final ChatSubscriptionCubit chatSubscriptionCubit;
  final BaseSocketSubscriptionCubit baseSocketSubscriptionCubit;
  
  void onReceivedSubscribedEvent(dynamic eventJson) {
    try{
      if(eventJson is GetInterlocoutorId){
        if(interlocutorId.isEmpty){
          interlocutorId = eventJson.id;
        }
      }
      if(eventJson is GetCreatorId){
        if(creatorId.isEmpty){
          creatorId = eventJson.id;
        }
      }
      if(eventJson is GetUserId){
        if(userId.isEmpty) {
          userId = eventJson.id;
        }
      }
      if(eventJson is CreatedTheme){
        emit(state.copyWith(selectTheme: eventJson.talkTheme));
        if(!userInChat) {
          chatSubscriptionCubit.initChat(currentId: state.getSelectedTheme!.id!);
        }
        return;
      }
      if(eventJson is ThemesFromSocket){
        emit(state.copyWith(themes: eventJson.themes, json: eventJson.toString()));
      }
      
    }catch(_){
      
    }
    
  }

  void addNewTheme({required TalkTheme talkTheme}){
    baseSocketSubscriptionCubit.send(event: 'addNewTheme', message: talkTheme);
  }

  void selectTheme({required TalkTheme talkTheme}){
    baseSocketSubscriptionCubit.send(event: 'selectTheme', message: talkTheme.id);
  }

  void deleteTheme({required TalkTheme talkTheme}){
    
    baseSocketSubscriptionCubit.send(event: 'deleteTheme', message: talkTheme.id);
  }

}


class ThemeSubscriptionState extends Equatable{
  final List<TalkTheme> _themes;
  final String _json;
  final TalkTheme? _selectedTheme;
  const ThemeSubscriptionState({List<TalkTheme> themes = const[], String json = "", bool isConnectionEstablished = false, TalkTheme? selectTheme}) : _themes = themes, _json = json, _selectedTheme = selectTheme, super();

  List<TalkTheme> get getThemes => _themes;

  String get getJson => _json;

  TalkTheme? get getSelectedTheme => _selectedTheme;

  @override
  List<Object?> get props => [_themes, _json, _selectedTheme];

  ThemeSubscriptionState copyWith({List<TalkTheme>? themes, String? json, bool? isConnectionEstablished, TalkTheme? selectTheme}) => ThemeSubscriptionState(themes: themes ?? _themes, json: json ?? _json, selectTheme: selectTheme);

}