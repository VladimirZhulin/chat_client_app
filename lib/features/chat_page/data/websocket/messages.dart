import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';

/// Сообщение
class MessageFromSocket {
  String? id;
  String? themeId;
  String? message;
  String? clientId;

  MessageFromSocket({this.id, this.themeId, this.message, this.clientId});

  MessageFromSocket.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    themeId = json['themeId'];
    message = json['message'];
    clientId = json['clientId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['themeId'] = themeId;
    data['message'] = message;
    data['clientId'] = clientId;
    return data;
  }

  bool get isOwn => clientId == userId;
}