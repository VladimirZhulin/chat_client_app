/// Сообщение для отправки в сокет
class SendMessage {
  int? id;
  String? themeId;
  String? message;

  SendMessage({this.id, this.themeId, this.message});

  SendMessage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    themeId = json['themeId'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['themeId'] = themeId;
    data['message'] = message;
    return data;
  }
}