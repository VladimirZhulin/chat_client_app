import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// Кубит, отвечающий за состояние того, печатает собеседник или нет
class TypingCubit extends Cubit<bool> {
  TypingCubit() : super(false);

  void update(bool isTyping) => emit(isTyping);
  void sendStatus(bool isTyping, {required String calleeId}) {
    if (isTyping) {
      channel!.emit("startTyping", {
        "calleeId": calleeId,
      });
    } else {
      channel!.emit("endTyping", {
        "calleeId": calleeId,
      });
    }
  }
}
