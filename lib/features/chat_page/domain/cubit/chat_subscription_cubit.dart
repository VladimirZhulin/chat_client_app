import 'package:chat_client_app/core/data/websocket/socket_event.dart';
import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/messages.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/send_message.dart';
import 'package:chat_client_app/core/socket/base_socket_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';
import 'package:chat_client_app/features/chat_page/domain/typing_cubit.dart';
import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// Кубит для работы с чатом. Необходим для:
/// 1) Отправки сообщения
/// 2) Подписки на сокет
class ChatSubscriptionCubit extends Cubit<BaseSocketSubscriptionState> {
  ChatSubscriptionCubit(
      {required this.chatSubscriptionWebSocketChannel,
      required this.typingCubit})
      : super(ChatNotInitState());

  final ChatSubscriptionWebSocketChannel chatSubscriptionWebSocketChannel;
  final TypingCubit typingCubit;

  /// Костыльный метод, при котором стейт возвращается в изначальное
  void dispose() => emit(ChatNotInitState());

  void initChat({required String currentId}) =>
      emit(UserNotConnectedState(currendThemeId: currentId));

  void onReceivedSubscribedEvent(eventJson) {
    try {
      if (state is UserNotConnectedState) {
        if (eventJson is UserJoinedInRoom) {
          if (eventJson.id == (state as UserNotConnectedState).currendThemeId) {
            userInChat = true;
            emit(UserConnectedState());
          }
        }
      }
      if (eventJson is MessagesFromSocket) {
        if (userInChat) {
          emit(UserGetMessages(messages: (eventJson).messages));
        }
      }
      if (eventJson is GetTypingStatus) {
        if (userInChat) {
          typingCubit.update(eventJson.isTyping);
        }
      }
      if (eventJson is Disconnect) {
        if (userInChat) {
          emit(UserDisconnected());
        }
      }
    } catch (_) {
      emit(UserConnectionErrorState());
    }
  }

  void leave(TalkTheme theme) =>
      chatSubscriptionWebSocketChannel.send(event: 'leave', message: theme);

  void sendMessage(SendMessage message) {
    try {
      chatSubscriptionWebSocketChannel.send(
          event: 'sendMessage', message: message);
    } catch (_) {
      emit(SendMessageError());
    }
  }
}

class ChatNotInitState extends BaseSocketSubscriptionState {}

class ChatInitState extends BaseSocketSubscriptionState {}

class UserNotConnectedState extends BaseSocketSubscriptionState {
  const UserNotConnectedState({required this.currendThemeId});
  final String? currendThemeId;

  @override
  List<Object?> get props => [...super.props, currendThemeId];

  @override
  UserNotConnectedState copyWith(
          {bool? isConnectionEstablished, String? currendThemeId}) =>
      UserNotConnectedState(currendThemeId: currendThemeId);
}

class UserConnectedState extends BaseSocketSubscriptionState {}

class UserConnectionErrorState extends BaseSocketSubscriptionState {}

class SendMessageError extends BaseSocketSubscriptionState {}

class IncomingCall extends BaseSocketSubscriptionState {}

class CallError extends BaseSocketSubscriptionState {}

class UserGetMessages extends BaseSocketSubscriptionState {
  final List<MessageFromSocket> messages;
  const UserGetMessages({required this.messages});

  @override
  List<Object?> get props => [...super.props, messages];

  @override
  UserGetMessages copyWith(
          {bool? isConnectionEstablished, List<MessageFromSocket>? messages}) =>
      UserGetMessages(messages: messages ?? []);
}

class UserDisconnected extends BaseSocketSubscriptionState {}
