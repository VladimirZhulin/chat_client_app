import 'dart:async';
import 'dart:developer';

import 'package:auto_route/auto_route.dart';
import 'package:chat_client_app/core/presentation/app_colors.dart';
import 'package:chat_client_app/core/presentation/app_ui.dart';
import 'package:chat_client_app/core/presentation/widgets/app_progress_indicator.dart';
import 'package:chat_client_app/core/presentation/widgets/widget_ext.dart';
import 'package:chat_client_app/core/routes/app_router.dart';
import 'package:chat_client_app/core/services/audio_service.dart';
import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:chat_client_app/di/injection_container.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/send_message.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';
import 'package:chat_client_app/features/chat_page/domain/typing_cubit.dart';
import 'package:chat_client_app/features/chat_page/presentation/widgets/message_item.dart';
import 'package:chat_client_app/features/themes_page/data/theme_actioons.dart';
import 'package:chat_client_app/core/socket/base_socket_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/domain/cubit/chat_subscription_cubit.dart';
import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

/// Страница чата

@RoutePage()
class ChatPage extends StatefulWidget {
  final TalkTheme theme;
  final ThemeAction action;
  const ChatPage({Key? key, required this.theme, required this.action})
      : super(key: key);

  @override
  State createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  late ThemeSubscriptionCubit _themeSubscriptionCubit;
  late ChatSubscriptionCubit _chatSubscriptionCubit;
  late TypingCubit _typingCubit;
  late AudioService _audioService;
  final TextEditingController _messageController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final ScrollController _scrollController = ScrollController();
  Timer? _typingTimer;
  dynamic incomingSDPOffer;

  @override
  void dispose() {
    _chatSubscriptionCubit.leave(widget.theme);
    if (_themeSubscriptionCubit.state.getSelectedTheme != null) {
      _themeSubscriptionCubit.deleteTheme(
          talkTheme: _themeSubscriptionCubit.state.getSelectedTheme!);
    }
    _chatSubscriptionCubit.dispose();
    userInChat = false;
    _audioService.stop();
    super.dispose();
  }

  void _onTextChanged() {
    if (_typingTimer != null && _typingTimer!.isActive) {
      _typingTimer!.cancel();
      _typingCubit.sendStatus(true, calleeId: _getCalleeId());
    }

    _typingTimer = Timer(const Duration(seconds: 1), () {
      _typingCubit.sendStatus(false, calleeId: _getCalleeId());
    });
  }

  @override
  void initState() {
    _audioService = locator.get<AudioService>();
    _themeSubscriptionCubit = locator.get<ThemeSubscriptionCubit>();
    _chatSubscriptionCubit = locator.get<ChatSubscriptionCubit>();
    _typingCubit = locator.get<TypingCubit>();
    _messageController.addListener(_onTextChanged);
    switch (widget.action) {
      case ThemeAction.create:
        _themeSubscriptionCubit.addNewTheme(talkTheme: widget.theme);
        break;
      case ThemeAction.select:
        userInChat = true;
        _themeSubscriptionCubit.selectTheme(talkTheme: widget.theme);
        break;
    }
    channel!.on("newCall", (data) {
      if (mounted) {
        // set SDP Offer of incoming call
        setState(() => incomingSDPOffer = data);
      }
    });
    channel!.on("callEnd", (data) {
      setState(() => incomingSDPOffer = null);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // Если звонок пошёл - проигрываем мелодию звонка
    if (incomingSDPOffer != null) {
      _audioService.playLocal('audio/ring.mp3', true);
    } else {
      _audioService.stop();
    }

    return Scaffold(
      appBar: _buildAppBar(),
      body: BlocBuilder<ChatSubscriptionCubit, BaseSocketSubscriptionState>(
          bloc: _chatSubscriptionCubit,
          builder: (BuildContext context, BaseSocketSubscriptionState state) {
            log(state.toString(), name: 'chat stare');
            if (state is UserDisconnected) {
              return _buildChatContent(state, true);
            }
            if (state is UserConnectedState ||
                state is UserGetMessages ||
                widget.action == ThemeAction.select) {
              return _buildChatContent(state);
            } else {
              if (state is UserNotConnectedState) {
                widget.theme.id = state.currendThemeId;
              }

              return _buildLoading();
            }
          }),
    );
  }

  AppBar _buildAppBar() =>
      AppUI.appBar(title: Text(widget.theme.getTitle), actions: [
        if (widget.theme.isCanRing == true)
          BlocBuilder<ChatSubscriptionCubit, BaseSocketSubscriptionState>(
            bloc: _chatSubscriptionCubit,
            builder: (context, state) => IconButton(
                onPressed: (state is UserConnectedState ||
                        widget.action == ThemeAction.select)
                    ? () => _makecall(
                        offer: incomingSDPOffer,
                        calleeId: _getCalleeId(),
                        callerId: userId)
                    : null,
                icon: const Icon(Icons.phone)),
          ).paddingOnly(right: AppUI.padding),
      ]);
  void _makecall(
      {required String callerId, required String calleeId, dynamic offer}) {
    log(calleeId, name: 'calleeId');
    context.router
        .push(CallRoute(callerId: callerId, calleeId: calleeId, offer: offer));
  }

  Widget _buildLoading() => const Center(
        child: AppProgressIndicator(),
      );
  Widget _buildChatContent(BaseSocketSubscriptionState state,
          [bool isChatOver = false]) =>
      Column(
        children: [
          if (incomingSDPOffer != null)
            ListTile(
              title: const Text(
                "Звонок от собеседника!",
              ),
              trailing: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  IconButton(
                    icon: const Icon(Icons.call_end),
                    color: Colors.redAccent,
                    onPressed: () {
                      channel!.emit("leaveCall", {
                        "calleeId": _getCalleeId(),
                      });
                      setState(() => incomingSDPOffer = null);
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.call),
                    color: Colors.greenAccent,
                    onPressed: () {
                      _audioService.playLocal('audio/answer_call.mp3');
                      _makecall(
                        callerId: incomingSDPOffer["callerId"]!,
                        calleeId: userId,
                        offer: incomingSDPOffer["sdpOffer"],
                      );
                      setState(() => incomingSDPOffer = null);
                    },
                  )
                ],
              ),
            ),
          Expanded(
              child: ListView(
            controller: _scrollController,
            //reverse нужен, чтобы список упирался вниз, таким образом, не придется при получении или отправки сообщения императивно прокручивать список вниз
            reverse: true,
            children: isChatOver
                ? [
                    const Align(
                        alignment: Alignment.centerLeft,
                        child: MessageItem(
                          text: "Собеседник завершил разговор",
                          isOwn: false,
                        )).paddingOnly(bottom: 8)
                  ]
                : state is UserGetMessages
                    ? state.messages.reversed
                        .map((e) => Animate(
                              key: ValueKey(e.id),
                              effects: const [FadeEffect(), ScaleEffect()],
                              child: Align(
                                      alignment: e.isOwn
                                          ? Alignment.centerRight
                                          : Alignment.centerLeft,
                                      child: MessageItem(
                                          text: e.message ?? "",
                                          isOwn: e.isOwn))
                                  .paddingOnly(bottom: 8),
                            ))
                        .toList()
                    : [],
          )),
          BlocBuilder<TypingCubit, bool>(
            bloc: _typingCubit,
            builder: (context, state) {
              if (state) {
                return Row(
                  children: [
                    Animate(
                      effects: const [
                        FadeEffect(duration: Duration(milliseconds: 200))
                      ],
                      child: const Text('Собеседник набирает сообщение...')
                          .paddingAll(AppUI.paddingXS),
                    ),
                  ],
                );
              } else {
                return const SizedBox();
              }
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(color: AppColors.appColor),
            child: Row(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width - 100,
                  child: TextField(
                    autocorrect: true,
                    autofocus: true,
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 5,
                    cursorColor: AppColors.appColor,
                    textCapitalization: TextCapitalization.sentences,
                    onSubmitted: (value) => _submitMessage(),
                    controller: _messageController,
                    focusNode: _focusNode,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.white1,
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                  ),
                ).paddingOnly(right: 20, left: 20),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: isChatOver == true ? null : _submitMessage,
                    child: SizedBox(
                      width: 55,
                      height: 55,
                      child: SvgPicture.asset(AppUI.sendIcon),
                    ),
                  ),
                )
              ],
            ).paddingSymmetric(vertical: 8),
          )
        ],
      );

  void _submitMessage() {
    if (_messageController.text.isNotEmpty) {
      _chatSubscriptionCubit.sendMessage(SendMessage(
          themeId: widget.theme.getId, message: _messageController.text));
      _messageController.text = "";
      _focusNode.requestFocus();
    }
  }

  String _getCalleeId() =>
      widget.action == ThemeAction.select ? creatorId : interlocutorId;
}
