import 'package:chat_client_app/core/presentation/app_colors.dart';
import 'package:chat_client_app/core/presentation/app_ui.dart';
import 'package:chat_client_app/core/presentation/widgets/widget_ext.dart';
import 'package:flutter/material.dart';

class MessageItem extends StatelessWidget {
  const MessageItem({ Key? key, required this.text, required this.isOwn}) : super(key: key);
  final String text;
  final bool isOwn;

  @override
  Widget build(BuildContext context){
    return Container(
      decoration: BoxDecoration(color: isOwn ? AppColors.appColor.shade400 : AppColors.appColor, borderRadius: BorderRadius.circular(10)),
      child: Text(text, style: Theme.of(context).textTheme.bodyMedium!.copyWith(color: AppColors.white1),).paddingAll(AppUI.paddingS)
    );
  }
}