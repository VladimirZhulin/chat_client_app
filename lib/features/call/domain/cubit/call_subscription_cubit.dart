import 'package:chat_client_app/core/data/websocket/socket_event.dart';
import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_webrtc/flutter_webrtc.dart';

class CallSubscriptionCubit extends Cubit<CallSubscriptionState>{
  CallSubscriptionCubit({required this.chatSubscriptionWebSocketChannel}):super(const CallSubscriptionState());
  final ChatSubscriptionWebSocketChannel chatSubscriptionWebSocketChannel;
  void onReceivedSubscribedEvent(eventJson) {
    if(eventJson is GetIceCandidate){
        String candidate = eventJson.data["iceCandidate"]["candidate"];
        String sdpMid = eventJson.data["iceCandidate"]["id"];
        int sdpMLineIndex = eventJson.data["iceCandidate"]["label"];
        emit(state.copyWith(candidate: RTCIceCandidate(candidate, sdpMid, sdpMLineIndex, )));
    }
    if(eventJson is CallAnswered){
      emit(CallSubscriptionState(sessionDescription: RTCSessionDescription(eventJson.data["sdpAnswer"]["sdp"],
            eventJson.data["sdpAnswer"]["type"],)));
    }
  }

  void call(data){
    try{
      chatSubscriptionWebSocketChannel.send(event: 'makeCall', message: data);
    }catch(_){
      //emit(CallError());
    }
  }

  void addIceCandidate(data){
    try{
      chatSubscriptionWebSocketChannel.send(event: 'IceCandidate', message: data);
    }catch(_){
      //emit(CallError());
    }
  }
  
}

class CallSubscriptionState extends Equatable{
  final RTCIceCandidate? candidate;
  final RTCSessionDescription? sessionDescription;
  const CallSubscriptionState({this.candidate, this.sessionDescription});
  
  @override
  List<Object?> get props => [candidate, sessionDescription];
  
  CallSubscriptionState copyWith({RTCIceCandidate? candidate, RTCSessionDescription? sessionDescription}) => CallSubscriptionState(candidate: candidate, sessionDescription: sessionDescription);

}