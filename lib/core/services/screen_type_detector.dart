import 'package:flutter/material.dart';

class ScreenTypeDetector {
  static ScreenType getFormFactor(BuildContext context) {
    // Use .shortestSide to detect device type regardless of orientation
    double deviceWidth = MediaQuery.of(context).size.shortestSide;
    if (deviceWidth > FormFactor.desktop) return ScreenType.desktop;
    if (deviceWidth > FormFactor.tablet) return ScreenType.tablet;
    return ScreenType.handset;
  }

  static bool isHandset(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    return deviceWidth <= FormFactor.tablet;
  }
}

enum ScreenType { handset, tablet, desktop }

class FormFactor {
  static const double desktopMax = 2560;
  static const double desktop = 900;
  static const double tablet = 600;
  static const double handset = 300;
}

