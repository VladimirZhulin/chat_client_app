import 'package:audioplayers/audioplayers.dart';

class AudioService {
  final audioPlayer = AudioPlayer();
  void playLocal(String source, [bool isLoop = false]) {
    if (isLoop) {
      audioPlayer.setReleaseMode(ReleaseMode.loop);
    } else {
      audioPlayer.setReleaseMode(ReleaseMode.release);
    }
    audioPlayer.stop();
    audioPlayer.play(AssetSource(source));
  }

  void stop() {
    audioPlayer.stop();
  }
}
