import 'package:chat_client_app/core/env.dart';

abstract class App {
  static const wsUrlMap = {
    AppEnv.test: 'ws://localhost:3001/',
    AppEnv.prod: 'wss://nekto-chat.ru:3001/'
  };
}

const currentEnv = AppEnv.prod;

String getUrl() => App.wsUrlMap[currentEnv] ?? '';
