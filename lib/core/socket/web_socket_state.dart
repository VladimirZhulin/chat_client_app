enum WebSocketState {
  /// Соединение установлено
  connected,

  /// Соединение в процессе
  connecting,

  /// Соединение разорвано из-за потери интернет соединения. Ожидает восстановления
  awaitingNetworkAvailability,

  /// Соединение разорвано
  disconnected,

  /// Сокет инициализирован
  initial
}
