import 'dart:async';

import 'package:chat_client_app/core/data/websocket/socket_event.dart';
import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:chat_client_app/core/socket/web_socket_state.dart';
import 'package:chat_client_app/di/injection_container.dart';
import 'package:chat_client_app/features/call/domain/cubit/call_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/domain/cubit/chat_subscription_cubit.dart';
import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

/// Базовый кубит для подключения и работы с WebSocket
/// Позволяет подписываться и отписываться

class BaseSocketSubscriptionCubit extends Cubit<BaseSocketSubscriptionState> {
  BaseSocketSubscriptionCubit(this.url, {required this.webSocket})
      : super(SocketInit()) {
    _initSocketConnection();
  }
  final String url;
  ChatSubscriptionWebSocketChannel webSocket;

  //ChatSubscriptionCubit chatSubscriptionCubit;
  //ThemeSubscriptionCubit themeSubscriptionCubit;

  void send({required String event, required dynamic message}) {
    webSocket.send(event: event, message: message);
  }

  Future<void> _initSocketConnection() async {
    if (state.isConnectionEstablished) {
      return;
    }
    try {
      await webSocket.connect();
    } catch (ex, stacktrace) {
      addError(ex, stacktrace);
    }
    webSocket.stateStream.listen(onWebSocketStateUpdate);
    webSocket.receivedSubscriptionMessages.listen(onReceivedSubscribedEvent);
  }

  void onWebSocketStateUpdate(WebSocketState wsState) {
    switch (wsState) {
      case WebSocketState.connected:
        emit(state.copyWith(isConnectionEstablished: true));
        break;
      case WebSocketState.awaitingNetworkAvailability:
        emit(state.copyWith(isConnectionEstablished: false));
        break;
      case WebSocketState.disconnected:
        emit(state.copyWith(isConnectionEstablished: false));
        // Пытаемся установить сокет соединение снова
        _initSocketConnection();
        break;
      case WebSocketState.initial:
      case WebSocketState.connecting:
        break;
    }
  }

  /// Вызывается при получении евента от сокет соединения.
  void onReceivedSubscribedEvent(SocketEvent event) {
    ChatSubscriptionCubit chatSubscriptionCubit =
        locator.get<ChatSubscriptionCubit>();
    ThemeSubscriptionCubit themeSubscriptionCubit =
        locator.get<ThemeSubscriptionCubit>();
    CallSubscriptionCubit callSubscriptionCubit =
        locator.get<CallSubscriptionCubit>();

    switch (event) {
      case GetUserId():
      case ThemesFromSocket():
      case CreatedTheme():
      case GetInterlocoutorId():
      case GetCreatorId():
        themeSubscriptionCubit.onReceivedSubscribedEvent(event);
      case UserJoinedInRoom():
      case MessagesFromSocket():
      case Disconnect():
      case GetTypingStatus():
        chatSubscriptionCubit.onReceivedSubscribedEvent(event);
      case GetIceCandidate():
      case CallAnswered():
        callSubscriptionCubit.onReceivedSubscribedEvent(event);
    }
  }
}

class BaseSocketSubscriptionState extends Equatable {
  const BaseSocketSubscriptionState({this.isConnectionEstablished = false});

  final bool isConnectionEstablished;

  @override
  List<Object?> get props => [isConnectionEstablished];

  BaseSocketSubscriptionState copyWith({bool? isConnectionEstablished}) =>
      BaseSocketSubscriptionState(
          isConnectionEstablished:
              isConnectionEstablished ?? this.isConnectionEstablished);
}

class SocketInit extends BaseSocketSubscriptionState {}
