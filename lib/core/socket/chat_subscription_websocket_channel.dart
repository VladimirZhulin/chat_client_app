import 'dart:async';
import 'dart:convert';

import 'package:chat_client_app/core/app.dart';
import 'package:chat_client_app/core/data/websocket/socket_event.dart';
import 'package:chat_client_app/core/socket/web_socket_state.dart';
import 'package:chat_client_app/core/utils/exceptions.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/messages.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';
import 'package:rxdart/rxdart.dart';
import 'package:socket_io_client/socket_io_client.dart';

Socket? channel;

class ChatSubscriptionWebSocketChannel {
  ChatSubscriptionWebSocketChannel({
    required this.url,
    this.reconnectDelay = defaultReconnectDelay,
  });

  /// Задержка между двумя попытками переподключения
  static const defaultReconnectDelay = Duration(seconds: 5);
  final String url;
  final Duration reconnectDelay;

  ValueStream<WebSocketState> get stateStream => _stateStream.stream;

  WebSocketState get state => stateStream.value;

  /// Максимальное количество переподключения
  static const maxReconnectTries = 10;

  /// Максимальное количество попыток отправки события по сокету
  static const maxResendTries = 10;

  /// Стрим полученных из сокет соединения сообщений, на которые мы подписались с помощью subscribe.
  /// Возвращает json мапу
  ValueStream<SocketEvent> get receivedSubscriptionMessages =>
      _subscrpbtion.stream;

  Future<void> connect() async {
    if (state == WebSocketState.initial ||
        state == WebSocketState.disconnected) {
      _reconnectTries = 0;
      _state = WebSocketState.connecting;
    }
    await _socketSubscription?.cancel();

    try {
      channel =
          io(getUrl(), OptionBuilder().setTransports(['websocket']).build());
      channel!.connect();
    } catch (ex) {
      print(
          '$url: Критическая ошибка при попытке установить с сокетом соединение, '
          'попытка #$_reconnectTries $ex');
      unawaited(_reconnect());
      throw SocketConnectionFailedException(
        url,
        'Критическая ошибка при попытке установить соединение, попытка #$_reconnectTries',
      );
    }
    try {
      channel!.on('message', (data) => _subscrpbtion.add(GetUserId(id: data)));
      channel!.on('interlocoutorId',
          (data) => _subscrpbtion.add(GetInterlocoutorId(id: data)));
      channel!
          .on('creatorId', (data) => _subscrpbtion.add(GetCreatorId(id: data)));
      channel!.on(
          'themes',
          (data) => _subscrpbtion.add(ThemesFromSocket(
              themes:
                  (data as List).map((e) => TalkTheme.fromJson(e)).toList())));
      channel!.on(
          'createdTheme',
          (data) => _subscrpbtion.add(CreatedTheme(
              talkTheme: TalkTheme.fromJson(jsonDecode(jsonEncode(data))))));
      channel!
          .on('join', (data) => _subscrpbtion.add(UserJoinedInRoom(id: data)));
      channel!.on(
          'messages',
          (data) => _subscrpbtion.add(MessagesFromSocket(
              messages: (data as List)
                  .map((e) => MessageFromSocket.fromJson(e))
                  .toList())));
      channel!.on("disconnected",
          (data) => _subscrpbtion.add(Disconnect(userId: data)));
      channel!.on(
          'typing',
          (data) =>
              _subscrpbtion.add(GetTypingStatus(isTyping: data['typing'])));
    } catch (e) {
      unawaited(_reconnect());
    }
  }

  /// Закрываем соединение. Можно будет перезапустить с помощью connect
  Future<void> close() async {
    assert(
      state != WebSocketState.disconnected,
      'InstrumentSubscriptionWebSocketChannel.close не может быть вызван на инстансе с состоянием '
      'WebSocketState.disconnected',
    );
    await _socketSubscription?.cancel();
    channel?.close();
    _state = WebSocketState.disconnected;
  }

  Future<void> _reconnect() async {
    await Future.delayed(reconnectDelay);
    if (state == WebSocketState.disconnected) return;

    _state = WebSocketState.connecting;
    _reconnectTries += 1;
    if (_reconnectTries > maxReconnectTries) {
      unawaited(close());
      throw SocketConnectionFailedException(
          url, 'Достигнут лимит попыток соединения с сокетом');
    }

    await connect();

    final currentReconnectTries = _reconnectTries;
    // Делаем проверку с задержкой чтобы убедиться,
    // что соединение установлено успешно и не разорволось снова через 5 секунд
    // Если разорвется, то мы не должны сбрасывать количество попыток переподключения
    Future.delayed(const Duration(seconds: 5), () {
      if (currentReconnectTries == _reconnectTries) {
        _reconnectTries = 0;
        _state = WebSocketState.connected;
      }
    });
  }

  void send({required String event, required dynamic message}) {
    channel!.emit(event, message);
  }

  /// Текущее количество неудачных попыток переподключения к сокету подряд.
  /// Сбрасываем при удачном подключении
  /// При достижении [maxReconnectTries] прекращаем пытаться переподключиться автоматически
  int _reconnectTries = 0;

  final _stateStream =
      BehaviorSubject<WebSocketState>.seeded(WebSocketState.initial);
  final _subscrpbtion = BehaviorSubject<SocketEvent>();
  set _state(WebSocketState state) => _stateStream.add(state);

  StreamSubscription? _socketSubscription;
}
