import 'package:chat_client_app/core/presentation/app_colors.dart';
import 'package:flutter/material.dart';

abstract class AppUI{
  static const paddingZero = 0.0;
  static const paddingXS = 8.0;
  static const paddingS = 12.0;
  static const padding = 16.0;
  static const paddingL = 32.0;
  static const paddingXL = 33.0;

  static const sendIcon = 'assets/icons/send_message.svg';

  static const progressIndicatorSize = 32.0;
  static const progressIndicatorSizeSmall = 16.0;
  static AppBar appBar({List<Widget>? actions, Widget? title }) => AppBar(actions: actions, title: title, centerTitle: true, );

  static showSnackBarError(BuildContext context, String message) {
    _showSnackBar(
      context,
      message,
      const Duration(seconds: 3),
      error: true,
    );
  }

  static _showSnackBar(
    BuildContext context,
    String message,
    Duration duration, {
    bool error = false,
  }) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        margin: const EdgeInsets.all(paddingXS),
        behavior: SnackBarBehavior.floating,
        duration: duration,
        content: error
            ? Text(
                message,
                style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                      color: Colors.white, // в темной теме на красном не виден текст
                    ),
              )
            : Text(message),
        backgroundColor: error ? AppColors.red1 : null,
      ),
    );
  }

}