import 'package:flutter/material.dart';

abstract class AppColors {
static const MaterialColor appColor = MaterialColor(_mcgpalette0PrimaryValue, <int, Color>{
  50: Color(0xFFE8F6F4),
  100: Color(0xFFC5E9E4),
  200: Color(0xFF9FDAD2),
  300: Color(0xFF79CBC0),
  400: Color(0xFF5CC0B3),
  500: Color(_mcgpalette0PrimaryValue),
  600: Color(0xFF39AE9D),
  700: Color(0xFF31A593),
  800: Color(0xFF299D8A),
  900: Color(0xFF1B8D79),
});
static const int _mcgpalette0PrimaryValue = 0xFF3FB5A5;

static const MaterialColor mcgpalette0Accent = MaterialColor(_mcgpalette0AccentValue, <int, Color>{
  100: Color(0xFFC6FFF4),
  200: Color(_mcgpalette0AccentValue),
  400: Color(0xFF60FFE0),
  700: Color(0xFF47FFDB),
});
static const int _mcgpalette0AccentValue = 0xFF93FFEA;

  static const Color white1 = Colors.white;
  static const Color red1 = Color.fromARGB(255, 200, 13, 0);

}