import 'package:chat_client_app/features/chat_page/data/websocket/messages.dart';
import 'package:chat_client_app/features/chat_page/data/websocket/talk_theme.dart';

sealed class SocketEvent {}

/// Получен id пользователя
class GetUserId implements SocketEvent {
  String id;
  GetUserId({required this.id});
}

class GetInterlocoutorId implements SocketEvent {
  String id;
  GetInterlocoutorId({required this.id});
}

class GetCreatorId implements SocketEvent {
  String id;
  GetCreatorId({required this.id});
}

/// Список тем, полученных их сокета
class ThemesFromSocket implements SocketEvent {
  List<TalkTheme> themes;
  ThemesFromSocket({required this.themes});
}

/// Пользователь зашел в комнату. id - id комнаты
class UserJoinedInRoom implements SocketEvent {
  String? id;

  UserJoinedInRoom({this.id});

  UserJoinedInRoom.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }
}

/// Пришел список сообщений из сокета
class MessagesFromSocket implements SocketEvent {
  final List<MessageFromSocket> messages;
  MessagesFromSocket({required this.messages});
}

/// Пользователь отключился
class Disconnect implements SocketEvent {
  String userId;
  Disconnect({required this.userId});
}

/// Создана новая тема
class CreatedTheme implements SocketEvent {
  TalkTheme talkTheme;
  CreatedTheme({required this.talkTheme});
}

/// Получение IceCandidate
class GetIceCandidate implements SocketEvent {
  dynamic data;
  GetIceCandidate({required this.data});
}

/// Ответ на звонок
class CallAnswered implements SocketEvent {
  dynamic data;
  CallAnswered({required this.data});
}

/// Печатает собеседник или нет
class GetTypingStatus implements SocketEvent {
  bool isTyping;
  GetTypingStatus({required this.isTyping});
}
