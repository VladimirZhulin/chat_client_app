import 'package:chat_client_app/core/app.dart';
import 'package:chat_client_app/core/services/audio_service.dart';
import 'package:chat_client_app/core/socket/base_socket_subscription_cubit.dart';
import 'package:chat_client_app/core/socket/chat_subscription_websocket_channel.dart';
import 'package:chat_client_app/features/call/domain/cubit/call_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/domain/cubit/chat_subscription_cubit.dart';
import 'package:chat_client_app/features/chat_page/domain/typing_cubit.dart';
import 'package:chat_client_app/features/themes_page/domain/cubit/theme_subscription_cubit.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

/// DI контейтер
Future<void> startup() async {
  locator.registerLazySingleton(
      () => ChatSubscriptionWebSocketChannel(url: getUrl()));
  locator.registerLazySingleton(() => ChatSubscriptionCubit(
      chatSubscriptionWebSocketChannel: locator(), typingCubit: locator()));
  locator.registerLazySingleton(() => ThemeSubscriptionCubit(
      chatSubscriptionCubit: locator(),
      baseSocketSubscriptionCubit: locator()));
  locator.registerLazySingleton(
      () => BaseSocketSubscriptionCubit(getUrl(), webSocket: locator()));
  locator.registerLazySingleton(
      () => CallSubscriptionCubit(chatSubscriptionWebSocketChannel: locator()));
  locator.registerLazySingleton(() => AudioService());
  locator.registerLazySingleton(() => TypingCubit());
}
